package ca.grantmanzano.loancalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import java.lang.*;
import java.util.regex.*;


public class MainActivity extends Activity {
    EditText loan;
    EditText termOfLoan;
    EditText yearlyInterestRate;
    TextView monthlyPayments;
    TextView totalPayment;
    TextView totalInterest;
    double loanNum;
    int numYears;
    double yearlyInterest;
    Button calculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get handle input fields
        loan = (EditText) findViewById(R.id.loan);
        termOfLoan = (EditText) findViewById(R.id.termOfLoan);
        yearlyInterestRate = (EditText) findViewById(R.id.yearlyInterestRate);

        //get handle on result fields
        monthlyPayments = (TextView) findViewById(R.id.monthlyPaymentNum);
        totalPayment = (TextView) findViewById(R.id.totalPaymentNum);
        totalInterest = (TextView) findViewById(R.id.totalInterestNum);

        //get handle on calculate button
        calculate = (Button) findViewById(R.id.calculate);
    }

    public void onCalculateBtnClick(View v) {
        loanNum = Double.parseDouble(loan.getText().toString());
        numYears = Integer.parseInt(termOfLoan.getText().toString());
        yearlyInterest = Double.parseDouble(yearlyInterestRate.getText().toString());

        if (loanNum <= 0 || loan.getText().toString().isEmpty()){
            monthlyPayments.setText("Invalid loan number.");
            return;
        }
        else if(numYears <= 0 || termOfLoan.getText().toString().isEmpty()){
            totalPayment.setText("Invalid term of loan.");
            return;
        }
        else if(yearlyInterest <= 0 || yearlyInterestRate.getText().toString().isEmpty() || !yearlyInterestRate.getText().toString().matches("^[0-9]+\\.*[0-9]*$")) {
            totalInterest.setText("Invalid yearly interest.");
            return;
        }
        else {
            LoanCalculator lc = new LoanCalculator(loanNum, numYears, yearlyInterest);

            //show results
            monthlyPayments.setText(Double.toString(lc.getMonthlyPayment()));
            totalPayment.setText(Double.toString(lc.getTotalCostOfLoan()));
            totalInterest.setText(Double.toString(lc.getTotalInterest()));
        }
    }

    public void onClearBtnClick(View v){
        //clear text on input text fields
        loan.setText("");
        termOfLoan.setText("");
        yearlyInterestRate.setText("");

        //clear text on result text fields
        monthlyPayments.setText("");
        totalPayment.setText("");
        totalInterest.setText("");
    }
}


